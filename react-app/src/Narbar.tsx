import React from 'react';
import { Nav } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import './Navbar.css';


interface INavbarStates {
    ticTacToeDropdown: boolean,
    timerDropdown: boolean
}

class Navbar extends React.Component<{}, INavbarStates> {

    constructor(props: {}) {
        super(props);
        this.state = {
            ticTacToeDropdown: false, 
            timerDropdown: false
        };
    }

    private toggle = (dropdownField: string) => {
        const dropdownState: any = { [dropdownField]: !(this.state as any)[dropdownField] };
        this.setState(dropdownState);
    }

    public render() {
        return (
            <Nav className="nav-bar">
                <NavLink to="/pizza" className="link">Pizza</NavLink>
                <NavLink to="/order" className="link">For staff</NavLink>
            </Nav>
        );
    }

}

export default Navbar;