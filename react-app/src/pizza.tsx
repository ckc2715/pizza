import React from 'react';
import { Button } from 'reactstrap';
import "./pizza.css";
import OrderSummary from './orderSummary';

interface Pizza {
    name: string,
    price: number,
    ingredients: []
}

export interface PizzaOrder {
    name: string,
    price: number,
    ingredients: [],
    quantity: number
}

interface IPizzaState {
    pizza: Pizza[],
    order: PizzaOrder[]
}


class ListPizza extends React.Component<{}, IPizzaState>{

    state: IPizzaState = {
        pizza: [],
        order: []
    }

    async componentDidMount() {
        let res = await fetch('http://localhost:8000/pizzas')
        let data = await res.json()

        this.setState({
            pizza: data
        })
    }

    cancel = (pizzaOrder:PizzaOrder)=>{
        const updateOrder = this.state.order.filter(pizza=>{
            return pizza.name !== pizzaOrder.name
        })
        this.setState({
            order:updateOrder
        })
    }

    order = (pizza: Pizza) => {

        if (this.state.order.find(pizzaOrder => pizzaOrder.name === pizza.name)) {
            let newOrder = this.state.order.slice()
            let target = newOrder.find(pizzaOrder => pizzaOrder.name === pizza.name)
            if (target){
            target.quantity +=1
            this.setState({
                order:newOrder
            })
        }
        } else {
            const newPizzaOrder: PizzaOrder = {
                ...pizza,
                quantity: 1
            }
            this.setState({
                order: this.state.order.concat(newPizzaOrder)
            })
        }
    }



    public render() {
        return (

            <div>
                <div className="pizzaTable">
                    Pizzas
                <div className="headRow" id="tag">
                        <div className="tidy">Name</div>
                        <div className="tidy">Price</div>
                        <div className="tidy">Ingredients</div>
                        <div className="tidy">Order</div>
                    </div>

                    {this.state.pizza.map((pizza, index) => {
                        return (
                            <div className="pizzaRow" key={index}>
                                <div className="item tidy">{pizza.name}</div>
                                <div className="item tidy">{pizza.price}</div>
                                <div className="item tidy">{pizza.ingredients}</div>
                                <Button className="item tidy orderButton" color="primary" onClick={this.order.bind(this, pizza)}>Order</Button>
                            </div>
                        )
                    })}

                </div>
                <OrderSummary cancel={this.cancel} order={this.state.order} />
            </div>
        )
    }
}


export default ListPizza