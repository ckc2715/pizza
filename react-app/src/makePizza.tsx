import React from 'react';
import "./pizza.css";

interface Pizza {
    name: string,
    price: number,
    ingredients: []
}

interface Quantity {
    name: string,
    quantity: number
}

export interface Order {
    id: string
    pizzas: Quantity[],
    status: string
}


interface IMakePizzaState {
    order: Order[]
}


class MakePizza extends React.Component<{}, IMakePizzaState>{

    state: IMakePizzaState = {
        order: []
    }

    async componentDidMount() {
        let res = await fetch('http://localhost:8000/order')
        let data = await res.json()

        this.setState({
            order: data
        })
    }

    public render() {
            const pending = this.state.order.filter(pizza=>pizza.status === 'pending')
            const preparing = this.state.order.filter(pizza=>pizza.status === 'preparing')
            const delivered = this.state.order.filter(pizza=>pizza.status === 'delivered')


        return (

            <div>
                <div className="pizzaTable">
                    Order
                <div className="headRow" id="tag">
                        <div className="tidy col-4">Pending
                        {pending.map(order=>{
                            return (
                                <div className="orderRow" key={order.id}>
                                    <div className="item tidy">Order ID:{order.id}</div>
                                    <div className="item tidy">Pizza:{order.pizzas && order.pizzas.map(pizza => {
                                        return (
                                            <ul>
                                                <li>Name:{pizza.name}</li>
                                                <li>Quantity:{pizza.quantity}</li>
                                            </ul>
                                        )
                                    })}</div>
                    
                                </div>
                            )
                        })}
                        
                        </div>
                        
                        <div className="tidy col-4">Preparing
                        {preparing.map(order=>{
                            return (
                                <div className="orderRow" key={order.id}>
                                    <div className="item tidy">Order ID:{order.id}</div>
                                    <div className="item tidy">Pizza:{order.pizzas && order.pizzas.map(pizza => {
                                        return (
                                            <ul>
                                                <li>Name:{pizza.name}</li>
                                                <li>Quantity:{pizza.quantity}</li>
                                            </ul>
                                        )
                                    })}</div>
                    
                                </div>
                            )
                        })}
                        </div>
                        
                        <div className="tidy col-4">Delivered
                        {delivered.map(order=>{
                            return (
                                <div className="orderRow" key={order.id}>
                                    <div className="item tidy">Order ID:{order.id}</div>
                                    <div className="item tidy">Pizza:{order.pizzas && order.pizzas.map(pizza => {
                                        return (
                                            <ul>
                                                <li>Name:{pizza.name}</li>
                                                <li>Quantity:{pizza.quantity}</li>
                                            </ul>
                                        )
                                    })}</div>
                    
                                </div>
                            )
                        })}</div>
                    </div>
{/* 
                    {this.state.order.map((order, index) => {
                        return (
                            <div className="pizzaRow" key={order.id}>
                                <div className="item tidy">{order.pizzas && order.pizzas.map(pizza => {
                                    return (
                                        <ul>
                                            <li>{pizza.name}</li>
                                            <li>{pizza.quantity}</li>
                                        </ul>
                                    )
                                })}</div>
                                <div className="item tidy">{order.status}</div>
                            </div>
                        )
                    })} */}

                </div>
            </div>
        )
    }
}


export default MakePizza