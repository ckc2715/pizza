import React from 'react';
import './App.css';
import ListPizza from './pizza';
import MakePizza from './makePizza';
import Navbar from './Narbar';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <BrowserRouter>
    <div className="App">
      <Navbar />
  
      <Switch>
          <Route path="/" exact={true} component={ListPizza} />
          <Route path="/pizza" component={ListPizza} />
          <Route path="/order" component={MakePizza} />
        </Switch>
        
      {/* <ListPizza />
      <MakePizza /> */}
    </div>
    </BrowserRouter>
  );
}

export default App;
