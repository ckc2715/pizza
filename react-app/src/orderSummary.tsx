import React from 'react';
import { Button } from 'reactstrap';
import { PizzaOrder } from './pizza';
import * as moment from 'moment'
// import "./pizza.css";

interface Pizza {
    name: string,
    price: number,
    ingredients: []
}

interface IPizzaState {
    pizza: Pizza[]
}

interface IOrderProps{
    order:PizzaOrder[],
    cancel: (pizzaOrder:PizzaOrder) => void
}

class OrderSummary extends React.Component<IOrderProps, IPizzaState>{

    state: IPizzaState = {
        pizza: []
    }

    async componentDidMount() {
        let res = await fetch('http://localhost:8000/pizzas')
        let data = await res.json()

        this.setState({
            pizza: data
        })
    }

    confirm = async()=>{
        const order = this.props.order.slice()
        const orderSummary = order.map(pizza=>{
            delete pizza.ingredients
            delete pizza.price
            return pizza
        })
        let res = await fetch('http://localhost:8000/order',{
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(orderSummary)
        })
        if (res.status === 200){
            const delivery = await res.json()
            let tempTime = moment.duration(delivery.deliveryTime);
            let time = tempTime.hours() + tempTime.minutes();
            console.log(`Your order will be delivered in ${time}  minutes`)
            alert(`Your order will be delivered in ${time} minutes`);
        }
    }

    public render() {
        return (
            <div className="orderTable">
              OrderSummary
              {this.props.order.map((pizza, index) => {
                            return (
                                <div className="pizzaRow" key={index}>
                                    <div className="item tidy">{pizza.name}</div>
                                    <div className="item tidy">{pizza.price}</div>
                                    <div className="item tidy">{pizza.ingredients}</div>
                                    <div className="item tidy">{pizza.quantity}</div>
                                    <Button className="item tidy orderButton" color="primary" onClick={this.props.cancel.bind(this,pizza)}>Cancel</Button>
                                </div>
                            )
                        })}
                        {this.props.order.length > 0 && <Button className="item tidy orderButton" color="primary" onClick={this.confirm}>Confirm</Button>}
                </div>
        )
    }
}


export default OrderSummary