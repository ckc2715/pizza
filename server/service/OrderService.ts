import * as jsonfile from "jsonfile";


interface Order {
    id: number,
    pizzas: Quantity[]
}

interface Delivery {
    success: boolean
    deliveryTime: number
}


interface Quantity {
    name: string,
    quantity: number
}

export class OrderService {

    protected orders: Order[]
    protected delivery: Delivery

    constructor() {
        this.loadOrder();
        this.loadDelivery();
    }

    public loadOrder() {
        this.orders = jsonfile.readFileSync(__dirname + "/orders.json")
        return this.orders
    }

    public loadDelivery() {
        this.delivery = jsonfile.readFileSync(__dirname + "/order.json")
        return this.delivery
    }

    public async newOrder(pizzas: Quantity[]) {
        const parsedOrderIds = this.orders.map((order) => order.id);
        const max = Math.max(...parsedOrderIds, 0);

        const newOrder = {
            id: max + 1,
            pizzas,
            status: 'pending'
        }
        this.orders.push(newOrder)
        await jsonfile.writeFile(__dirname + "/orders.json", this.orders)
        return this.delivery
    }
}