import * as jsonfile from "jsonfile";

interface Pizza {
    name: string,
    price: number,
    ingredients: []
}


export class PizzaService {


    protected pizzas:Pizza[]


    constructor() {
        this.loadPizzas();
    }


    public loadPizzas(){
        this.pizzas = jsonfile.readFileSync(__dirname + "/pizzas.json")
        return this.pizzas
    }
    
}