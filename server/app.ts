import * as express from "express";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as cors from 'cors'
import { PizzaService } from "./service/PizzaService";
import { PizzaRouter } from "./router/PizzaRouter";
import { OrderService } from "./service/OrderService";
import { OrderRouter } from "./router/OrderRouter";


export const pizzaService = new PizzaService();
export const orderService = new OrderService();
export const pizzaRouter = new PizzaRouter();
export const orderRouter = new OrderRouter();

const app = express();
const server = new http.Server(app)
const PORT = 8000

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}))

app.use('/pizzas', pizzaRouter.router());
app.use('/order', orderRouter.router());


server.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));