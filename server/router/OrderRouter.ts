import * as express from 'express';
import { orderService } from "../app"
import { OrderService } from '../service/OrderService';

export class OrderRouter {
    private orderService: OrderService

    constructor() {
        this.orderService = orderService
    }

    public router() {
        const router = express.Router();
        router.get('/', this.loadOrder)
        router.post('/', this.newOrder)

        return router
    }


    loadOrder = async (req: express.Request, res: express.Response) => {
        res.json(this.orderService.loadOrder())
    }

    newOrder = async (req: express.Request, res: express.Response) => {
        res.json(await this.orderService.newOrder(req.body))
    }
}