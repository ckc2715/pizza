import * as express from 'express';
import { PizzaService } from '../service/PizzaService';
import { pizzaService} from "../app"

export class PizzaRouter {
   private pizzaService:PizzaService

   constructor(){
       this.pizzaService = pizzaService
   }

   

    public router (){
        const router = express.Router();
        router.get('/', this.getPizzas)
        return router
    }


    getPizzas = async (req: express.Request, res: express.Response) => {
            res.json(this.pizzaService.loadPizzas())
    }
}